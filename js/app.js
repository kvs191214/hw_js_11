
// 1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>,
//   створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const sectionContent = document.querySelector('#content');
const clickMeButton = document.querySelector('#btn-click');

clickMeButton.addEventListener('click', (event) =>{
    const newParagraph = document.createElement('p');
    newParagraph.innerText = "New Paragraph";
    sectionContent.append(newParagraph);
})

// 2. Додати новий елемент форми із атрибутами:
//    Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути,
//    наприклад, type, placeholder, і name. та додайте його під кнопкою.

const footer = document.querySelector('footer');

const addFormSection = document.createElement('section');
addFormSection.id = 'new-forms';

const addForm = document.createElement("form");
addForm.id = 'form'

const addFormButton = document.createElement('input');
addFormButton.type = 'button';
addFormButton.id = 'btn-input-create';
addFormButton.value = 'New input';

footer.before(addFormSection);
addFormSection.append(addForm);
addForm.prepend(addFormButton);

addFormButton.addEventListener('click', (event) => {
    const newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.placeholder = 'Enter your text';
    newInput.name = 'new input';
    addFormButton.after(newInput);
})





