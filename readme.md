<b>Теоретичні питання</b>

1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

****
<b>Відповіді :</b>

1. Подія у JavaScript це сигнал від браузера що щось сталося. Щоб виконати дію (код) на потрібну подію необхідно для
   події вказати функцію яка буде виконуватися коли настане подія.
2. У JavaScript доступні такі події мищі
   як : `click`, `contextmenu`, `mouseover`, `mouseout`, `mousedown`, `mouseup`, `mousemove`. Приклади : <br>
   <br>
    - `const button = document.querySelector('button');`<br>
      `let count = 0;`<br>
      `const domCount = document.querySelector('#button-count');`<br>
      `button.addEventListener('click', (event) =>` <br>
      `count++;` <br>
      `domCount.innerText = count;`<br>
      `})`<br>
      <br>
    - `btn.addEventListener('mouseover', () =>{` <br>
      `alert('Натискаючи сюди ви увійдете в систему');` <br>
      `}, {once: true});` <br>
      <br>

3. `contextmenu` - коли користувач правою кнопкою миші клікає на елемент. Ця подія дає можливість 
   динамічно відображати контекстне меню, яке залежить від елемента, на який було натиснуто. Можна відмінити
   contextmenu , що б при натисканні на пкм воно не виникало або створити своє кастомне контексте меню.